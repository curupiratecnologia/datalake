from django.db import models
from django_cryptography.fields import encrypt
from django.core import validators
from metadata.validators import list_validator

DRIVER_CHOICES = [
    ('postgresql+psycopg2', 'PostgreSQL'),
    ('mysql', 'MySQL'),
    ('oracle+cx_oracle', 'Oracle'),
    ('mssql+pyodbc', 'Microsoft SQL Server'),
    ('sqlite', 'SQLite'),
]

ATUALIZATION_CHOICES= [
    ('ALL', 'Replace all records',),
    ('ADD', 'Add new records',),
    ('UP',  'Update changed records and Add new records ')
    ]

FILE_TYPE_CHOICES = [
    ('csv','Comma-separated Values'),
    ('fwf', 'Table of fixed-width formatted lines'),
    ('xls', ' Excel file, xls and xlsx'),
    ('json', 'JSON object')]


class Info(models.Model):
    name = models.CharField(max_length=500)
    description = models.TextField(null=True, blank=True)

    def __str__(self):
        return self.name

    class Meta:
        abstract = True

class Access(models.Model):
    user = models.CharField(max_length=100, null=True, blank=True)
    password = encrypt(models.CharField(max_length=100, null=True, blank=True))

    def __str__(self):
        return self.user

    class Meta:
        abstract = True

class CronParams(Info):
    cron_string = models.CharField(max_length=100)

    def __str__(self):
        return self.name


class SSHTunel(Info, Access):
    host = models.CharField(max_length=500)
    port = models.IntegerField()

    def __str__(self):
        return self.name

class ResponsibleParty(Info):
    email = models.EmailField(null=True, blank=True)
    telephone = models.CharField(max_length=100, null=True, blank=True)
    point_of_contact = models.CharField(max_length=100, null=True, blank=True)
    def __str__(self):
        return '%s - %s' %(self.name, self.point_of_contact) if self.point_of_contact else self.name

class AtualizationParams(models.Model):
    atualization_type = models.CharField(max_length=3, choices=ATUALIZATION_CHOICES)
    identifier_field = models.CharField(max_length=100, null=True, blank=True)
    reference_field_for_update = models.CharField(max_length=100, null=True, blank=True)
    cron = models.ForeignKey(CronParams, on_delete=models.CASCADE)
    class Meta:
        abstract = True

class DataSource(Info, Access, AtualizationParams):
    responsible_party = models.ForeignKey(ResponsibleParty, on_delete=models.SET_NULL, null=True, blank=True)
    ssh = models.ForeignKey(SSHTunel, on_delete=models.SET_NULL, null=True, blank=True)
    target_name = models.CharField(max_length=500)


class FileInformation(models.Model):
    file_type = models.CharField(max_length=4, choices=FILE_TYPE_CHOICES)
    delimiter = models.CharField(max_length=1, default=',')
    quote = models.CharField(max_length=1, default='"')
    has_header = models.BooleanField(default=True)
    header = models.CharField(max_length=5000, validators=[list_validator], null=True, blank=True)
    column_width = models.CharField(max_length=5000, validators=[validators.validate_comma_separated_integer_list], null=True, blank=True)

    class Meta:
        abstract = True        



class Database(DataSource):
    host = models.CharField(max_length=2048)
    port = models.IntegerField()
    drive = models.CharField(max_length=100, choices=DRIVER_CHOICES)
    table = models.CharField(max_length=100)
    sql = models.TextField()

    def __str__(self):
        return self.name
        

class RESTApi(DataSource):
    url = models.URLField()

    def __str__(self):
        return self.name

class RESTApiParams(models.Model):
    rest_api =  models.ForeignKey(RESTApi, on_delete=models.CASCADE)
    key = models.CharField(max_length=100)
    value = models.CharField(max_length=500)

class FTP(DataSource, FileInformation):
    url = models.CharField(max_length=2048)

class HTTP(DataSource, FileInformation):
    url = models.CharField(max_length=2048)

class StaticFile(DataSource,FileInformation):
    pass

class Files(Info):
    static_file = models.ForeignKey(StaticFile, on_delete=models.CASCADE)
    _file = models.FileField(upload_to='')    