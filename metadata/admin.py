from django.contrib import admin

from metadata.models import CronParams, SSHTunel, Database, RESTApi,\
    RESTApiParams, FTP, HTTP, StaticFile, Files, ResponsibleParty

from django.forms import ModelForm, PasswordInput, CharField

# Register your models here
admin.site.register(CronParams)
admin.site.register(SSHTunel)
admin.site.register(ResponsibleParty)



class GenericForm(ModelForm):
    password = CharField(widget=PasswordInput(), required=False)
    class Meta:
        fields = '__all__'

class DatabaseForm(ModelForm):
    password = CharField(widget=PasswordInput(), required=True)
    class Meta:
        fields = '__all__'


class RESTApiParamsInline(admin.TabularInline):
    model = RESTApiParams
    extra = 3


class RESTApiAdmin(admin.ModelAdmin):
    fieldsets = [
        ('Identification',  {'fields': ['name', 'description', 'responsible_party']}),
        ('Connection',      {'fields': ['url', 'user', 'password', 'ssh']}),
        ('Atualization',      {'fields': ['atualization_type','identifier_field', 'reference_field_for_update', 'cron']}),
        ('Target',      {'fields': ['target_name']}),

    ]
    list_display = ('name', 'atualization_type', 'cron','responsible_party')
    list_filter = ('atualization_type', 'cron__name', 'responsible_party__name')
    form = GenericForm
    inlines = [RESTApiParamsInline]


admin.site.register(RESTApi, RESTApiAdmin)


class DatabaseAdmin(admin.ModelAdmin):
    fieldsets = [
        ('Identification',  {'fields': ['name', 'description', 'responsible_party']}),
        ('Connection',      {'fields': ['drive','host', 'port', 'user', 'password', 'ssh']}),
        ('Data',      {'fields': ['table', 'sql']}),
        ('Atualization',      {'fields': ['atualization_type','identifier_field', 'reference_field_for_update', 'cron']}),
        ('Target',      {'fields': ['target_name']}),

    ]
    list_display = ('name', 'atualization_type', 'cron','responsible_party')
    list_filter = ('atualization_type', 'cron__name', 'responsible_party__name')
    form = DatabaseForm


admin.site.register(Database, DatabaseAdmin)


class FTPHTTPAdmin(admin.ModelAdmin):
    fieldsets = [
        ('Identification',  {'fields': ['name', 'description', 'responsible_party']}),
        ('Connection',      {'fields': ['url', 'user', 'password', 'ssh']}),
        ('Data',      {'fields': ['file_type', 'delimiter', 'quote', 'has_header', 'header', 'column_width']}),
        ('Atualization',      {'fields': ['atualization_type','identifier_field', 'reference_field_for_update', 'cron']}),
        ('Target',      {'fields': ['target_name']}),

    ]
    list_display = ('name', 'atualization_type', 'cron','responsible_party')
    list_filter = ('atualization_type', 'cron__name', 'responsible_party__name')
    form = GenericForm


admin.site.register(FTP, FTPHTTPAdmin)
admin.site.register(HTTP, FTPHTTPAdmin)



class FilesInline(admin.TabularInline):
    model = Files
    extra = 3


class StatiscFileAdmin(admin.ModelAdmin):
    fieldsets = [
        ('Identification',  {'fields': ['name', 'description', 'responsible_party']}),
        ('Data',      {'fields': ['file_type', 'delimiter', 'quote', 'has_header', 'header', 'column_width']}),
        ('Atualization',      {'fields': ['atualization_type','identifier_field', 'reference_field_for_update', 'cron']}),
        ('Target',      {'fields': ['target_name']}),

    ]
    list_display = ('name', 'atualization_type', 'cron','responsible_party')
    list_filter = ('atualization_type', 'cron__name', 'responsible_party__name')
    form = GenericForm
    inlines = [FilesInline]

admin.site.register(StaticFile, StatiscFileAdmin)

