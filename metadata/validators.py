import re
from django.core import validators

def list_validator(sep=',', message='Enter a list separated by commas.', code='invalid'):
    regexp = validators._lazy_re_compile(r'(.+?)(?:%(sep)s|$)' % {'sep': re.escape(sep),})
    return validators.RegexValidator(regexp, message=message, code=code)

